--Practical task number 1
insert into film (film_id,title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length ,replacement_cost, rating,last_update)
values('1001','GOD FATHER','This is a movie about mafia','1972','1','2','14','4.99' ,'235','19.99','NC-17','2023-10-22 19:16:14.629 +0500' );

insert into actor (actor_id,first_name,last_name,last_update)
values('201','MARLON','BRANDO','2017-02-15 11:34:33.000 +0500'),
	  ('202','AL','PACHINO','2017-02-15 11:34:33.000 +0500'),
	  ('203','ROBERT','DUVALL','2017-02-15 11:34:33.000 +0500'),
	  ('204','DIANE','KEATON','2017-02-15 11:34:33.000 +0500');

insert into film_actor (actor_id,film_id,last_update)
values ('201','1001','2017-02-15 12:05:03.000 +0500'),
	   ('202','1001','2017-02-15 12:05:03.000 +0500'),
	   ('203','1001','2017-02-15 12:05:03.000 +0500'),
	   ('204','1001','2017-02-15 12:05:03.000 +0500');
	  
insert into inventory (inventory_id,film_id,store_id)
values('4582','1001','2');

--Practical task number 2
update film as f
set rental_duration='21',
    rental_rate ='9.99'
where f.film_id ='1001';

update customer as c
set first_name ='SHAZOD',
	last_name ='ZOHIDOV',
	email ='Shahzod_Zohidov@student.itpu.uz',
	create_date ='2023-10-22'
where c.customer_id ='318';
 
select customer_id, count(*)  as count 
from payment p 
group by customer_id 
having count(*)>=10 
order by count asc;

--Practical task number 3
delete 
from film_actor 
where film_id ='1001';

delete 
from film 
where film_id = '1001';

delete 
from payment as p
where p.customer_id ='318';

delete 
from rental as r
where r.customer_id ='318';
--Practical task number 1
insert into film (film_id,title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length ,replacement_cost, rating,last_update)
values('1001','GOD FATHER','This is a movie about mafia','1972','1','2','14','4.99' ,'235','19.99','NC-17','2023-03-08 13:26:14.629 +0500' );

insert into actor (actor_id,first_name,last_name,last_update)
values('201','MARLON','BRANDO','2017-02-15 11:34:33.000 +0500'),
	  ('202','AL','PACHINO','2017-02-15 11:34:33.000 +0500'),
	  ('203','ROBERT','DUVALL','2017-02-15 11:34:33.000 +0500'),
	  ('204','DIANE','KEATON','2017-02-15 11:34:33.000 +0500');

insert into film_actor (actor_id,film_id,last_update)
values ('201','1001','2017-02-15 12:05:03.000 +0500'),
	   ('202','1001','2017-02-15 12:05:03.000 +0500'),
	   ('203','1001','2017-02-15 12:05:03.000 +0500'),
	   ('204','1001','2017-02-15 12:05:03.000 +0500');
	  
insert into inventory (inventory_id,film_id,store_id)
values('4582','1001','2');




 
   

